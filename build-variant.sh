#!/bin/bash

die() {
	ra=$1
	shift
	echo "$*" >&2
	exit ${ra}
}

tag=$1
IFS=- read -ra _opts <<< ${tag}

if [ ${#_opts[@]} -ne 2 ]; then
	die 1 "Invalid tag ${tag}"
fi

python_version=${_opts[0]}
cv_version=${_opts[1]}

case "${python_version}" in
	2|3) ;;
	*)
		die 1 "Bad Python version ${python_version}"
esac

# hardware architecture or board
arch=armv7hf

sed \
	-e "s@#{ARCH}@${arch}@g" \
	-e "s@#{CV_VERSION}@${cv_version}@g" \
	-e "s@#{PYTHON_VERSION}@${python_version}@g" \
	Dockerfile.tpl > Dockerfile.${tag}

docker build --pull \
	-f Dockerfile.${tag} -t ${arch}-opencv:${tag} \
	$(dirname $0)

# TODO: Make optional
rm Dockerfile.${tag}
