FROM quay.io/ethanwu10/#{ARCH}-numpy:#{PYTHON_VERSION} as builder

# Keep at top for caching
RUN apt-get update && \
    apt-get install -y \
            libpng12-0 libtiff5 libjasper1 libavcodec56 libavformat56 \
            libswscale3 libv4l-0 libv4l2rds0 libv4lconvert0           \
            libxvidcore4 libx264-142 libatlas3-base

RUN apt-get install -y \
            build-essential cmake pkg-config libjpeg-dev libpng12-dev \
            libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev \
            libswscale-dev libv4l-dev libxvidcore-dev libx264-dev     \
            libatlas-base-dev gfortran

RUN mkdir /build/
WORKDIR /build/
ENV OPENCV_VERSION="#{CV_VERSION}"
RUN wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.tar.gz \
         -O opencv.tar.gz -nv && \
    tar -xzf opencv.tar.gz && \
    wget https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.tar.gz \
         -O opencv_contrib.tar.gz -nv && \
    tar -xzf opencv_contrib.tar.gz
RUN mkdir opencv-${OPENCV_VERSION}/build
WORKDIR opencv-${OPENCV_VERSION}/build
RUN cmake \
          -D WITH_CUDA=OFF \
          -D WITH_TBB=ON \
          -D WITH_V4L=ON \
          -D BUILD_WITH_DEBUG_INFO=OFF \
          -D BUILD_TESTS=OFF \
          -D BUILD_PERF_TESTS=OFF \
          -D BUILD_EXAMPLES=OFF \
          -D ENABLE_VFPV3=ON \
          -D ENABLE_NEON=ON \
          -D CMAKE_BUILD_TYPE=Release \
          -D CMAKE_INSTALL_PREFIX=/build/output \
          -D OPENCV_EXTRA_MODULES_PATH="/build/opencv_contrib-${OPENCV_VERSION}/modules" \
          ..
RUN make -j $(nproc)
RUN make install

FROM quay.io/ethanwu10/#{ARCH}-numpy:#{PYTHON_VERSION}

# Keep at top for caching
RUN apt-get update && \
    apt-get install -y \
            libpng12-0 libtiff5 libjasper1 libavcodec56 libavformat56 \
            libswscale3 libv4l-0 libv4l2rds0 libv4lconvert0           \
            libxvidcore4 libx264-142 libatlas3-base

COPY --from=builder /build/output /usr/local

RUN ldconfig
